﻿using AutoMapper;
using Connection;
using RRShop.ApiModels;
using RRShop.Models;

namespace RRShop.App_Start
{
    public class MapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<ApplicationUser, InformationViewModels>();
            Mapper.CreateMap<CartItemApiModels, CartShop>();
        }
    }
}