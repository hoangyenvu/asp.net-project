﻿using Business.Business;
using Business.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRShop.Models
{
    public class CartShopViewModels
    {
        public string CartID { get; set; }

        public List<CartShopSqlView> CartItems { get; set; }

        public short CartTotal { get; set; }

        public long Total { get; set; }

        public string UserId { get; set; }

        public static CartShopViewModels GetCurrentCart()
        {
            CartShopViewModels cart = new CartShopViewModels();

            if (HttpContext.Current.Request.Cookies["User"] == null)
            {
                if (HttpContext.Current.Request.Cookies["CartShop"] == null)
                {
                    cart.CartID = Guid.NewGuid().ToString();

                    HttpContext.Current.Response.Cookies["CartShop"].Value = cart.CartID;

                    cart.CartItems = new List<CartShopSqlView>();
                    cart.Total = 0;
                    cart.CartTotal = 0;
                }
                else
                {
                    cart.CartID = HttpContext.Current.Request.Cookies["CartShop"].Value;
                    cart.CartItems = CartShopSqlViewBUS.GetCartShopById(cart.CartID).ToList();
                    cart.Total = cart.CartItems.Sum(c => c.Price * c.Quantity);
                    cart.CartTotal = (short)cart.CartItems.Sum(c => c.Quantity);
                }
            }
            else
            {
                cart.UserId = HttpContext.Current.Request.Cookies["UserId"].Value;
                cart.CartID = HttpContext.Current.Request.Cookies["CartShop"].Value;
                cart.CartItems = CartShopSqlViewBUS.GetCartShopByUserId(cart.UserId).ToList();
                cart.Total = cart.CartItems.Sum(c => c.Price * c.Quantity);
                cart.CartTotal = (short)cart.CartItems.Sum(c => c.Quantity);
            }

            return cart;
        }
    }
}
