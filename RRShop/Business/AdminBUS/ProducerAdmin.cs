﻿using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class ProducerAdmin
    {
        public static IEnumerable<Producer> LoadAllProducer()
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT ProducerID, ProducerName, RouteName, Logo
                                    FROM Producer
                                    WHERE IsRemoved = 0";
            var list = db.Query<Producer>(queryString);

            return list;
        }

        public static int AddProducer(Producer producer)
        {
            Object item;
            producer.RouteName = producer.ProducerName.ToUnsign();
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Producer", "ProducerID", producer);
            }
            int id = int.Parse(item.ToString());

            return id;
        }

        public static bool UpdateProducer(Producer producer)
        {
            var db = new ConnectionDB();
            producer.RouteName = producer.ProducerName.ToUnsign();

            IEnumerable<string> str = new string[] { "ProducerName", "RouteName" };
            int row = db.Update(producer, producer.ProducerID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteProducer(int ProducerID)
        {
            var db = new ConnectionDB();
            int row = db.Update<Producer>("SET IsRemoved=1 Where ProducerID=@0", ProducerID);

            if (row == 0)
                return false;

            return true;
        }

        public static Producer GetProducerByProducerID(int ProducerID)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT ProducerID, ProducerName, RouteName
                                    FROM Producer
                                    WHERE ProducerID = @0 AND IsRemoved = 0";
            var producer = db.SingleOrDefault<Producer>(queryString, ProducerID);

            return producer;
        }

        public static bool UpdateLogoProducer(Producer producer)
        {
            var db = new ConnectionDB();
            producer.RouteName = producer.ProducerName.ToUnsign();

            IEnumerable<string> str = new string[] { "Logo" };
            int row = db.Update(producer, producer.ProducerID, str);

            if (row == 0)
                return false;

            return true;
        }
    }
}
