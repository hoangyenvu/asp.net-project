﻿using Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Comment")]
    public class CommentController : Controller
    {
        [Route("_CommentPartial")]
        public ActionResult _CommentPartial(int? page, int proId, string proName)
        {
            ViewBag.ProName = proName;
            ViewBag.ProId = proId;

            return View(CommentBUS.GetCommentsByProductId(page, proId));
        }
    }
}