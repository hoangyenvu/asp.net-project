﻿using System.Web;
using System.Web.Optimization;

namespace RRShop
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        //"~/Scripts/prototype/prototype.js",
                        //"~/Scripts/lib/ccard.js",
                        //"~/Scripts/lib/validation.js",
                        //"~/Scripts/lib/builder.js",
                        //"~/Scripts/lib/effects.js",
                        //"~/Scripts/lib/dragdrop.js",
                        //"~/Scripts/lib/controls.js",
                        //"~/Scripts/lib/slider.js",
                        //"~/Scripts/lib/js.js",
                        //"~/Scripts/lib/menu.js",
                        //"~/Scripts/lib/ma.jq.slide.js",
                        "~/Scripts/myJS/formatNumber.js",
                        "~/Scripts/jquery-ui-1.11.4.min.js",
                        "~/Scripts/lib/owl.carousel.js",
                        "~/Scripts/lib/ma.bxslider.min.js",
                        "~/Scripts/lib/ma.mobilemenu.js",
                        "~/Scripts/lib/backtotop.js",
                        //"~/Scripts/lib/ma.lettering.js",
                        //"~/Scripts/lib/ma.ajaxlogin.js",
                        "~/Scripts/lib/custommenu.js",
                        //"~/Scripts/lib/ma_quickview.js",
                        "~/Scripts/lib/timer.js",
                        "~/Scripts/lib/ma.flexslider.js",
                        "~/Scripts/lib/ma.nivo.js"
                        //"~/Scripts/lib/jquery.bpopup.min.js",
                        //"~/Scripts/lib/jquery.easing.1.3.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/myScript").Include(
                        "~/Scripts/myJS/owlCarousel.js",
                        "~/Scripts/myjS/cart-shop.js",
                        "~/Scripts/myJS/detail.js",
                        "~/Scripts/myJS/bxSlider.js",
                        "~/Scripts/bootstrap-datepicker.js",
                        "~/Scripts/bootstrap-datepicker.vi.min.js",
                        "~/Scripts/myJS/semantic/transition.min.js",
                        "~/Scripts/myJS/semantic/dropdown.min.js",
                        "~/Scripts/myJS/ion-rangeSlider/ion.rangeSlider.min.js", // js ion range slider          
                        //"~/Scripts/myJS/jquery.ui.shake.min.js",
                        "~/Scripts/myJS/mustache/mustache.min.js",
                        "~/Scripts/myJS/flyToCart.js",
                        "~/Scripts/myJS/tooltip.js",
                        "~/Scripts/myJS/account.js"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/open-sans.css",
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-theme.css",
                        "~/Content/styles.css",
                        "~/Content/owl.carousel.css",
                        "~/Content/font-awesome/css/font-awesome.min.css",
                        "~/Content/widgets.css",
                        "~/Content/animate.css",
                        "~/Content/bootstrap-datepicker3.min.css",
                        "~/Content/ma-css/ma_quickview.css",
                        "~/Content/skin.css",
                        "~/Content/ma-css/ma.banner7.css",
                        "~/Content/testimonial.css",
                        "~/Content/testimonial_slider.css",
                        "~/Content/semantic/transition.min.css",
                        "~/Content/semantic/dropdown.min.css",
                        "~/Content/my_styles.css",
                        "~/Content/ion-rangeSlider/ion.rangeSlider.css",
                        "~/Content/ion-rangeSlider/ion.rangeSlider.skinHTML5.css" // css ion range slider
                        ));
        }
    }
}
