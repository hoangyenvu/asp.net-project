﻿$(document).ready(function () {
    $("#btnComment").on("click", function () {
        var check = true;
        var name = $("#nickname");
        var comment = $("#comment");
        var proId = $(this).attr("data-proId");

        if (name.val() === "") {
            name.nextAll().text(name.attr("data-val-required"));
            check = false;
        }

        if (comment.val() === "") {
            comment.nextAll().text(comment.attr("data-val-required"));
            check = false;
        }

        if (check) {
            $.post(urlComment, { ProductID: proId, Name: name.val(), _Comment: comment.val() }, function (data) {
                var paging = $("#product_tabs_product_additional_data_contents .toolbar-bottom .paging");
                paging.empty();

                var comments = $(".ui.comments");
                comments.empty();

                for (var i = 0, n = data.Items.length; i < n; i++) {
                    var element = data.Items[i];

                    var comment = Mustache.render($("#comment_template").html(),
                                  { name: element.Name, date: moment(element.PostedDate).format("DD/MM/YYYY hh:mm:ss A"), comment: element._Comment });

                    comments.append(comment);
                }

                for (var i = 1, n = data.TotalPages; i <= n; i++) {
                    var page = Mustache.render($("#page_template").html(), { page: i, "class": i === data.CurrentPage ? "current" : "" });
                    paging.append(page);
                }

                if (data.CurrentPage < data.TotalPages) {
                    var next = Mustache.render($("#next_template").html(), { page: (data.CurrentPage + 1) });
                    paging.append(next);
                }

                comment.val("");
            });
        }
    });

    $("#product_tabs_product_additional_data_contents .toolbar-bottom .paging").on("click", "li > a", function () {
        var $this = $(this);
        if ($this.hasClass("current") === false) {
            var page = $this.text() !== "" ? $this.text() : $this.attr("data-page");

            $.post(urlChangePage, { Page: page, ProductId: $("#btnComment").attr("data-proId") }, function (data) {
                var paging = $("#product_tabs_product_additional_data_contents .toolbar-bottom .paging");
                paging.empty();

                if (data.CurrentPage > 1) {
                    var back = Mustache.render($("#back_template").html(), { page: (data.CurrentPage - 1) });
                    paging.append(back);
                }

                for (var i = 1, n = data.TotalPages; i <= n; i++) {
                    var page = Mustache.render($("#page_template").html(), { page: i, "class": i === data.CurrentPage ? "current" : "" });
                    paging.append(page);
                }

                if (data.CurrentPage < data.TotalPages) {
                    var next = Mustache.render($("#next_template").html(), { page: (data.CurrentPage + 1) });
                    paging.append(next);
                }

                var comments = $(".ui.comments");
                comments.empty();

                for (var i = 0, n = data.Items.length; i < n; i++) {
                    var element = data.Items[i];

                    var comment = Mustache.render($("#comment_template").html(),
                                  { name: element.Name, date: moment(element.PostedDate).format("DD/MM/YYYY hh:mm:ss A"), comment: element._Comment });

                    comments.append(comment);
                }
            });
        }
    });
});