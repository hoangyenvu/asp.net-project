﻿using Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Product")]
    public class ProductController : Controller
    {
        [Route("{id}/{name}")]
        public ActionResult Detail(int id = 0, string name = "")
        {
            if (id == 0 || name == "")
            {
                return RedirectToAction("Index", "Home");
            }

            ProductViewModelsBUS.UpdateViewedForProduct(id);

            return View(ProductViewModelsBUS.GetProductById(id));
        }

        public ActionResult _RelatedCategoryPartial(byte id, int productID)
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetRelatedCategoryProducts(id, productID));
        }

        public ActionResult _RelatedProducerPartial(short id, int productID)
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetRelatedProducerProducts(id, productID));
        }
    }
}