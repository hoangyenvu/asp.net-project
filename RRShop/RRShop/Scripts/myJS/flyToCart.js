﻿$(document).ready(function () {
    $(".add-to-box-cart .btn-cart").on("click", function () {
        var cart = $("#mini_cart_block");
        var imgtodrag = $(this).parent().parent().parent().prev().find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '1000000'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 30,
                    'left': cart.offset().left + 30,
                    'width': 75,
                    'height': 75
                }, 1000, 'easeInOutExpo');

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });

            var proId = $(this).attr("data-proId");
            var quantity = $(this).parent().find("#qty").val();
            var price = unformatNumber($(this).parent().parent().prev().find("span.price").text());

            $.post(urlAdd, { ProductID: proId, Quantity: quantity, Price: price }, function (data) {
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                subTotal.text(formatNumber(data.Total));

                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");
            });
        }
    });

    $('.item-inner .btn-cart').on('click', function () {
        var cart = $("#mini_cart_block");
        var imgtodrag = $(this).parent().parent().find("img").eq(0);
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                    top: imgtodrag.offset().top,
                    left: imgtodrag.offset().left
                })
                .css({
                    'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '1000000'
                })
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top + 30,
                    'left': cart.offset().left + 30,
                    'width': 75,
                    'height': 75
                }, 1000, 'easeInOutExpo');

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });

            var proId = $(this).attr("data-proId");
            var quantity = 1;
            var price = unformatNumber(imgtodrag.parent().parent().find("span.price").text());

            $.post(urlAdd, { ProductID: proId, Quantity: quantity, Price: price }, function (data) {
                var quantity = cart.find(".count-item");
                quantity.text(data.CartTotal);

                var subTotal = cart.find(".top-subtotal > span.price > span");
                subTotal.text(formatNumber(data.Total));

                quantity.stop(true, true).removeAttr("style").effect("shake", { times: 2, distance: 5, direction: "up" }, "slow");
            }, "json");
        }
    });
});