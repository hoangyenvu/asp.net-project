﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class PromotionAdmin
    {
        public static Page<Sale> LoadAllSale(short page)
        {
            var db = new ConnectionDB();
            string queryString = @"Select SaleID, StartDate, DueDate, Description
                                    From Sale
                                    Order By StartDate Desc";

            var list = db.Page<Sale>(page, 10, queryString);

            return list;
        }

        public static Page<SaleDetailViewModel> LoadSaleDetailByID(int id, short page)
        {
            var db = new ConnectionDB();
            string queryString = @"Select SD.SaleDetailID, SD.SaleID, SD.ProductID, P.ProductName, SD.DiscountID, S.Description, D.Value, P.Price, D.Discount
                                    From SaleDetail SD, Product P, Sale S, Discount D
                                    Where S.SaleID = @0 and 
                                            SD.SaleID = S.SaleID and
		                                    P.ProductID = SD.ProductID and
                                            SD.DiscountID = D.DiscountID";

            var list = db.Page<SaleDetailViewModel>(page, 10, queryString, id);

            return list;
        }

        public static bool AddSale(Sale sale)
        {
            Object item;
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Sale", "SaleID", sale);
            }

            if (item != null)
            {
                return true;
            }

            return false;
        }

        public static bool UpdateSale(Sale sale)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "StartDate", "DueDate", "Description"};
            int row = db.Update(sale, sale.SaleID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static IEnumerable<ProductViewModels> SearchForInsertSale(SearchItems item)
        {

            var sql = Sql.Builder.Append(@"Select P.ProductID, ProductName, cat.CategoryID, cat.CategoryName, SUB.SubCategoryID, SUB.SubCategoryName, PC.ProducerID, PC.ProducerName, Price, P.RouteName
                                                        From Producer PC, Product P
		                                                        LEFT JOIN Category cat on p.CategoryID = cat.CategoryID
		                                                        LEFT JOIN SubCategory SUB ON SUB.CategoryID = cat.CategoryID and sub.SubCategoryID = p.SubCategoryID and SUB.IsRemoved = 0 and cat.IsRemoved = 0
                                                        Where P.ProducerID = PC.ProducerID AND
                                                                P.IsRemoved = 0 AND PC.IsRemoved = 0  ");
            if (item.Keyword != null && item.Keyword != "")
            {
                item.Keyword = item.Keyword.ToUnsign();
                //item.Keyword = " %" + item.Keyword + "%";
                sql.Append(" AND P.RouteName like @0","%" + item.Keyword + "%");
            }

            if (item.CategoryID > 0)
            {
                sql.Append(" AND cat.CategoryID = @0", item.CategoryID);
            }

            if (item.SubCategoryID > 0)
            {
                sql.Append(" AND SUB.SubCategoryID = @0", item.SubCategoryID);
            }

            if (item.ProducerID > 0)
            {
                sql.Append(" AND PC.ProducerID = @0", item.ProducerID);
            }

            var db = new ConnectionDB();
            //string s = sql.ToString();
            var result = db.Query<ProductViewModels>(sql);
            

            return result;
        }


        public static bool InsertSaleDetail(SaleDetail sd)
        {
            Object item;
            using (var db = new ConnectionDB())
            {
                item = db.Insert("SaleDetail", "SaleDetailID", sd);
            }
            int id = int.Parse(item.ToString());
            if (id > 0)
                return true;
            return false;
        }


        public static bool DeleteSaleDetail(int SaleDetailID)
        {
            var db = new ConnectionDB();
            int row = db.Delete("SaleDetail", "SaleDetailID", null, SaleDetailID);

            if (row == 0)
                return false;

            return true;
        }


        public static bool UpdateSaleDetail(SaleDetail sd)
        {
            var db = new ConnectionDB();

            IEnumerable<string> str = new string[] { "DiscountID" };
            int row = db.Update(sd, sd.SaleDetailID, str);

            if (row == 0)
                return false;

            return true;
        }









        public static IEnumerable<Discount> LoadAllDiscount()
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT DiscountID, Discount, Value
                                    FROM Discount
                                    Order By Discount Asc";
            var list = db.Query<Discount>(queryString);

            return list;
        }

        public static bool InsertDiscount(Discount discount)
        {
            Object item;
            discount.Value = discount.Value = ((int)(discount._Discount * 100)).ToString() + '%';
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Discount", "DiscountID", discount);
            }
            int id = int.Parse(item.ToString());
            if (id > 0)
                return true;
            return false;
        }

        public static bool UpdateDiscount(Discount discount)
        {
            var db = new ConnectionDB();
            discount.Value = discount.Value = ((int)(discount._Discount * 100)).ToString() + '%';
            IEnumerable<string> str = new string[] { "Discount", "Value" };
            int row = db.Update(discount, discount.DiscountID, str);

            if (row == 0)
                return false;

            return true;
        }

        public static bool DeleteDiscount(int DiscountID)
        {
            var db = new ConnectionDB();
            int row = db.Delete("Discount", "DiscountID", null, DiscountID);

            if (row == 0)
                return false;

            return true;
        }
    }
}
