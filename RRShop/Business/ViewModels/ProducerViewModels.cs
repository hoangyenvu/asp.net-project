﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class ProducerViewModels
    {
        [Column]
        public short ProducerID { get; set; }

        [Column]
        public string ProducerName { get; set; }

        [Column]
        public string RouteName { get; set; }

        [Column]
        public string Logo { get; set; }
    }
}
