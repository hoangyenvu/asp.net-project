﻿using Newtonsoft.Json;
using RRShop.ApiModels;
using System.Net;
using System.Web;
using System.Web.Http;

namespace RRShop.WebApi
{
    [RoutePrefix("CaptchaApi")]
    public class CaptchaApiController : ApiController
    {
        [Route("Confirm")]
        [HttpPost]
        public object Confirm([FromBody]CaptchaApiModels captcha)
        {
            var client = new WebClient();
            var json = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}",
                "6LeEQCMTAAAAAKz8kwjSWVqsWZajUP9aQp-ZdmH8", captcha.Response));
            var result = JsonConvert.DeserializeObject(json);
            return result;
        }
    }
}
