﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RRShop.ApiModels
{
    public class CartItemApiModels
    {
        public int ProductID { get; set; }
        
        public short Quantity { get; set; }

        public uint Price { get; set; }
    }
}