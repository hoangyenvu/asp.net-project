﻿using Business.Business;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ClassBody = "cms-index-index cms-home";
            ViewBag.HasSale = Library.HasSale();

            return View();
        }

        public ActionResult _FeatureCategoryPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(CategoryViewModelsBUS.GetCategoriesWithImage());
        }

        public ActionResult _TopNewPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetTopNewProducts());
        }

        public ActionResult _TopSellerPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetTopSellerProducts());
        }

        public ActionResult _TopSalePartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetTopSaledProducts());
        }

        public ActionResult _MostViewedPartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetTopViewedProducts());
        }

        public ActionResult _HotSalePartial()
        {
            if (!ControllerContext.IsChildAction)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(ProductViewModelsBUS.GetHotSaleProducts());
        }
    }
}