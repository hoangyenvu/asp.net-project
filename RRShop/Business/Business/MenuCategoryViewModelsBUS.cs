﻿using Business.ViewModels;
using Connection;
using System.Collections.Generic;
using System.Linq;

namespace Business.Business
{
    public class MenuCategoryViewModelsBUS
    {
        /// <summary>
        /// Gets Category List with Sub Category List per Category 
        /// </summary>
        /// <returns></returns>
        public static List<MenuCategoryViewModels> GetCategories()
        {
            List<MenuCategoryViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select CategoryID, CategoryName, RouteName From Category Where IsRemoved = @0";

                result = db.Query<MenuCategoryViewModels>(query, 0).ToList();
            }

            foreach (var element in result)
            {
                element.SubCategories = SubCategoryViewModelsBUS.GetSubCategoriesByCategory(element.CategoryID).ToList();
            }

            return result;
        }
    }
}
