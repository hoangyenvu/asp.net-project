﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RRShop.Startup))]
namespace RRShop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
