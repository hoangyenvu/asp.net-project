﻿using Business.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Controllers
{
    [RoutePrefix("Sale")]
    public class SaleController : Controller
    {
        [Route("{page?}")]
        public ActionResult Index(int? page)
        {
            return View(ProductViewModelsBUS.GetSaledProducts(page));
        }
    }
}