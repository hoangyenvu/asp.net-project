﻿using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.AdminBUS
{
    public class ImageAdmin
    {
        public static bool InsertImage(Image img)
        {
            var db = new ConnectionDB();
            int row = (int)db.Insert("Image", img);

            if (row > 0)
                return true;

            return false;
        }

        public static IEnumerable<Image> GetImageByProID(int id)
        {
            var db = new ConnectionDB();
            string queryString = @"Select ImageID, Link, ProductID, IsMainImage
                                    From Image
                                    Where ProductID = @0";
            var list = db.Query<Image>(queryString, id);

            return list;
        }

        public static bool DeleteImage(Image img)
        {
            var db = new ConnectionDB();
            int row = db.Delete("Image", "ImageID", img);

            if (row > 0)
                return true;

            return false;
        }

        public static void SetAllFalse(int ProID)
        {
            var db = new ConnectionDB();
            int row = db.Update<Image>("SET IsMainImage=0 Where ProductID=@0", ProID);

        }

        public static bool SetMainImg(int ImgID)
        {
            var db = new ConnectionDB();
            int row = db.Update<Image>("SET IsMainImage=1 Where ImageID=@0", ImgID);

            if (row > 0)
                return true;

            return false;
        }
    }
}
