﻿using Business.ViewModels;
using Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Business
{
    public class CategoryViewModelsBUS
    {
        /// <summary>
        /// Gets Category List without Image
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<CategoryViewModels> GetCategories()
        {
            IEnumerable<CategoryViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select CategoryID, CategoryName, RouteName From Category Where IsRemoved = @0";

                result = db.Query<CategoryViewModels>(query, 0);
            }

            return result;
        }

        /// <summary>
        /// Gets Category List with Image per Category
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<CategoryViewModels> GetCategoriesWithImage()
        {
            IEnumerable<CategoryViewModels> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select CategoryID, CategoryName, Image, RouteName From Category Where IsRemoved = @0";

                result = db.Query<CategoryViewModels>(query, 0);
            }

            return result;
        }

        /// <summary>
        /// Gets Category List with Banner
        /// </summary>
        /// <param name="id">Category ID</param>
        /// <returns></returns>
        public static CategoryViewModels GetCategoriesWithBanner(int id)
        {
            CategoryViewModels result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select CategoryID, CategoryName, Banner, RouteName From Category Where CategoryID = @0";

                result = db.SingleOrDefault<CategoryViewModels>(query, id);
            }

            return result;
        }

        /// <summary>
        /// Gets Category by Id
        /// </summary>
        /// <param name="id">Category's Id</param>
        /// <returns></returns>
        public static CategoryViewModels GetCategoryById(int id)
        {
            CategoryViewModels result;

            using (var db = new ConnectionDB())
            {
                result = db.SingleOrDefault<CategoryViewModels>(@"Select CategoryID, CategoryName, RouteName
                                                                  From Category
                                                                  Where CategoryID = @0", id);
            }

            return result;
        }
    }
}
