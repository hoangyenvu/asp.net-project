﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class CartShopSqlView
    {
        [Column]
        public int ProductId { get; set; }

        [Column]
        public string ProductName { get; set; }

        [Column]
        public short Quantity { get; set; }

        [Column]
        public int Price { get; set; }

        [Column]
        public short RealQuantity { get; set; }
    }
}
