﻿using Business.ViewModels;
using Connection;
using PetaPoco;
using System;

namespace Business.Business
{
    public class CommentBUS
    {
        public static Page<Comment> GetCommentsByProductId(int? page, int proId)
        {
            Page<Comment> result;

            using (var db = new ConnectionDB())
            {
                string query = @"Select Name, Comment, PostedDate, UserID
                                 From Comment
                                 Where ProductID = @0 
                                 Order by PostedDate DESC";

                result = db.Page<Comment>(page.HasValue ? page.Value : 1, 5, query, proId);
            }

            return result;
        }

        public static void InsertComment(Comment comment)
        {
            using (var db = new ConnectionDB())
            {
                string query = @"Insert into Comment(ProductID, UserID, Name, Comment, PostedDate) 
                                 Values(@0, @1, @2, @3, @4)";

                db.Execute(query, comment.ProductID, comment.UserID, comment.Name, comment._Comment, comment.PostedDate);
            }
        }
    }
}
