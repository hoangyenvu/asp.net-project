﻿using Business.Business;
using Business.ViewModels;
using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Business.AdminBUS
{
    public class ProductAdmin
    {
        public static Page<ProductViewModels> LoadProductByPage(int id)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT P.ProductID, P.ProductName, P.CategoryID, P.SubCategoryID, P.Price, P.UploadDate, P.RouteName, PC.ProducerName, CAT.CategoryName, SUB.SubCategoryName
                                    FROM Producer PC, Product p LEFT JOIN Category cat on p.CategoryID = cat.CategoryID
	                                    LEFT JOIN SubCategory SUB ON SUB.CategoryID = cat.CategoryID and sub.SubCategoryID = p.SubCategoryID and SUB.IsRemoved = 0 and cat.IsRemoved = 0
                                    WHERE P.ProducerID = PC.ProducerID AND
                                            P.IsRemoved = 0 AND PC.IsRemoved = 0 ";
            var list = db.Page<ProductViewModels>(id, 20, queryString);
           
            return list;
        }

        public static ProductViewModelForAdmin GetProductByID(int id)
        {
            var db = new ConnectionDB();

            string queryString = @"SELECT P.ProductID, P.ProductName, P.ProducerID, P.CategoryID, P.SubCategoryID, P.Price, P.UploadDate, P.Quantity, P.RouteName, P.Description,PC.ProducerName, CAT.CategoryName, SUB.SubCategoryName
                                    FROM Producer PC, Product p LEFT JOIN Category cat on p.CategoryID = cat.CategoryID
	                                    LEFT JOIN SubCategory SUB ON SUB.CategoryID = cat.CategoryID and sub.SubCategoryID = p.SubCategoryID and SUB.IsRemoved = 0 and cat.IsRemoved = 0
                                    WHERE P.ProductID =  @0 AND
                                            P.IsRemoved = 0 AND PC.IsRemoved = 0 AND
                                            P.ProducerID = PC.ProducerID";
            ProductViewModelForAdmin ProVM = db.SingleOrDefault<ProductViewModelForAdmin>(queryString, id);
            return ProVM;
        }

        public static bool UpdateProduct(Product pro)
        {
            var db = new ConnectionDB();
            if (pro.ProductName != "")
            {
                pro.RouteName = pro.ProductName.ToUnsign();
            }
            else
                pro.RouteName = "empty";
                  
            IEnumerable<string> str = new string[] { "ProductName", "ProducerID", "CategoryID", "SubCategoryID", "UploadDate", "Quantity", "Price", "Description", "RouteName"};
            //int row = db.Update(pro, pro.ProductID, str);
            int row = db.Update("Product", "ProductID", pro, str);
            if (row == 0)
                return false;

            return true;
        }

        public static string GetProductNameByID(int id)
        {
            var db = new ConnectionDB();
            string queryString = @"SELECT ProductName
                                    FROM Product
                                    WHERE ProductID = @0";
            string proName = db.SingleOrDefault<string>(queryString, id);
            return proName;
        }

        public static bool AddProduct(Product Product)
        {
            Object item;
            if (Product.ProductName != "")
            {
                Product.RouteName = Product.ProductName.ToUnsign();
            }
            else
                Product.RouteName = "empty";
            
            using (var db = new ConnectionDB())
            {
                item = db.Insert("Product", "ProductID", Product);
            }

            if (item != null)
            {
                return true;
            }
            return false;
        }

        public static bool DeleteProduct(int ProID)
        {
            var db = new ConnectionDB();
            int row = db.Update<Product>("SET IsRemoved=1 Where ProductID=@0", ProID);

            if (row == 0)
                return false;

            return true;
        }
    }
}
