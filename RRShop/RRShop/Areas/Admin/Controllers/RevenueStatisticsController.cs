﻿using Business.AdminBUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace RRShop.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RevenueStatisticsController : Controller
    {
        // GET: Admin/RevenueStatistics
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDataDate(string[] days)
        {
            List<MyData> myData = new List<MyData>();
            for (int i = 0; i < days.Length; i++)
            {
                MyData item = new MyData();
                item.title = days[i];
                item.value = RevenueStatisticAdmin.statisticsForDate(days[i]);

                myData.Add(item);
            }

            return Json(myData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataMonth(short year)
        {
            List<MyData> myData = new List<MyData>();
            for (int i = 0; i < 12; i++)
            {
                MyData item = new MyData();
                item.title = "Tháng " + (i + 1).ToString();
                item.value = RevenueStatisticAdmin.statisticsForMonth((short)(i + 1), year);

                myData.Add(item);
            }

            return Json(myData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataQuarter(short year)
        {
            List<MyData> myData = new List<MyData>();
            for (int i = 0; i < 4; i++)
            {
                MyData item = new MyData();
                item.title = "Quý " + (i + 1).ToString();
                int count = 0;
                for (int j = 0; j < 3; j++)
                {
                    count += RevenueStatisticAdmin.statisticsForMonth((short)(i * 3 + j + 1), year);
                }
                item.value = count;

                myData.Add(item);
            }

            return Json(myData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataYear(short year)
        {
            List<MyData> myData = new List<MyData>();
            for (int i = -3; i <= 0; i++)
            {
                MyData item = new MyData();
                item.title = "Năm " + (year + i).ToString();
                item.value = RevenueStatisticAdmin.statisticsForYear((short)(year + i));

                myData.Add(item);
            }

            return Json(myData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataProducer()
        {
            return Json(RevenueStatisticAdmin.statisticsForProducer(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDataCategory()
        {
            return Json(RevenueStatisticAdmin.statisticsForCategory(), JsonRequestBehavior.AllowGet);
        }
        
    }


    public class MyData
    {
        public string title { get; set; }
        public int value { get; set; }
    }
}