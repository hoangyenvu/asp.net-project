﻿using Connection;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ViewModels
{
    public class ProductViewModelForAdmin
    {
        [Column]
        public int ProductID { get; set; }

        [Column]
        public int CategoryID { get; set; }

        [Column]
        public int SubCategoryID { get; set; }


        [Column]
        public int ProducerID { get; set; }

        [Column]
        [Required(ErrorMessage = "Tên sản phẩm không được rỗng")]
        public string ProductName { get; set; }

        [Column]
        public string RouteName { get; set; }

        [Column]
        public int Sold { get; set; }

        [Column]
        [Required(ErrorMessage = "Giá sản phẩm không được rỗng")]
        [Range(10000, long.MaxValue, ErrorMessage = "Giá tiền thấp nhất cho phép là 10.000")]
        public int Price { get; set; }

        [Column]
        public int Viewed { get; set; }

        [Column]
        [Required(ErrorMessage = "Số lượng sản phẩm không được rỗng")]
        public int Quantity { get; set; }

        [Column]
        public string Description { get; set; }

        [Column]
        public string ProducerName { get; set; }

        [Column]
        public string CategoryName { get; set; }

        [Column]
        public DateTime UploadDate { get; set; }

        [Column]
        public string CategoryRouteName { get; set; }

        [Column]
        public string SubCategoryName { get; set; }

        [Column]
        public string SubCategoryRouteName { get; set; }

        [Column]
        public DateTime? DueDate { get; set; }

        public List<Image> Images { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
